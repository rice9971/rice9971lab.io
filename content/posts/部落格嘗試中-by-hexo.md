---
title: 部落格嘗試中 - by hexo
date: 2019-04-06 13:33:31
tags:
- 其他
---
前陣子沒什麼動力，認真思考人生後，覺得自己喜歡的大概是寫程式和教人吧！所以打算在練習程式和做專案時，可以用部落格紀錄當下的想法！
<!--more-->
### 平台
原本想過 [WordPress](https://tw.wordpress.org/), [Blogger](https://www.blogger.com/), [Medium](https://medium.com/) 甚至是自己寫一個，稍微對需求做了點分析：

1. 不想租空間或使用到個人伺服器。
2. 希望可以客製化一個乾淨、整齊的版面，但不想自己寫前端。
3. 自訂 domain。

就發現似乎都不太適合，此時我想到 GitLab Page。

### 靜態網頁產生器
大概就 Jekyll 和 Hexo 在思考，由於本人後端慣 Node.js，而且前者編譯很慢，所以毫不猶豫選擇 Hexo！

這時候，我發現有個打競賽的朋友，他的部落格使用了 Hugo，但在 plugin 安裝上沒有 Hexo 那麼方便，就維持原本的決定了。

基本上 Hexo 和 Hugo 都支援 Markdown，在寫文章方面應該是沒什麼大問題。

### Hexo
有問題翻一下[文件](https://hexo.io/zh-tw/)，其實蠻詳細的。

Theme 的部分選用 [Nayo](https://github.com/Lemonreds/hexo-theme-Nayo)，有個小插曲是，我一直找不到 Telgram 的 Icon，而那部分似乎是原作者自己加工過的，我就半夜三點寄 email 過去問，早上睡醒發現收到回覆，作者把 TG 的 Icon 加工過後，直接推了呢XD

基本上 Hexo 在發布文章、儲存草稿、加入標籤和分類...等部落格該有的基本功能上，做得還算齊。

### GitLab CI
由於個人會做一些可能有版權問題的爬蟲專案，而GitLab 能開免費的 private 專案，所以我喜歡 GitLab 遠大於 GitHub。

以 Hexo on GitLab 來說，要使用 CI 的步驟蠻簡單的。

1. 在 blog 根目錄建立一個 .gitlab-ci.yml
2. 修改裡面的內容為

```
image: node:${你的 node 版本}
pages:
  cache:
    paths:
    - node_modules/
  script:
  - npm install hexo-cli -g
  - npm install
  - hexo deploy
  artifacts:
    paths:
    - public
  only:
  - master
```
3. 推上去

這樣每次都會自動更新，而不用手動去處理發布的問題了！

### 最後
希望這不會是我的最後一篇文章QQ

預期自己會先整理一下這學期修的課的作業和自己的整理，另外因為自己有在刷題目(9成個人興趣+1成課堂需求)，也會放一下解題的想法和過程！