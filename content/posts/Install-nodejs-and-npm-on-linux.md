---
title: Install nodejs and npm on linux
date: 2019-08-20 00:42:31
tags:
- Linux
- Ubuntu
- Node.js
---
本文簡介如何在 ubuntu 安裝 nodejs 和 npm
<!--more-->

#### 前言
此方法適合嫌麻煩的人或不太會指令的新手。

若為不喜歡 ` curl <url> | bash - ` 的朋友，可以移除原本的 ppa，加入 nodesource 的 signing key 及 repository，apt update 後就可以安裝了。

#### 前置作業
本文使用 terminal，請先按下 ctrl + alt + t 叫出他。

第一步為安裝 curl:

`sudo apt install curl
`

#### 查看目前的 node.js 版本
進入[官網](https://nodejs.org/)，看目前的版本，個人都使用最新的，也有人習慣使用較穩定的 LTS。

這裡我們需要的是大版本號，例如 10.16.3 就是 10；12.8.1 就是 12。

#### 在本地執行 nodesource 的 setup file
基本上就是抓 deb.nodesource.com/ 裡面的 setup file，檔名為 setup_${版本}.x。curl 用來抓取，抓完後直接用 root 執行。實際指令：

`curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash  - `

#### 安裝
直接 apt install 即可。
`
sudo apt install -y nodejs
`

#### 確認
用 -v 指令，若出現版本號則安裝成功。
` node -v `
` npm -v `