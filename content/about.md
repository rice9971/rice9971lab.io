---
title: about
layout: about
date: 2019-04-05 20:50:13
---
HI, I am Rice, a student who is passionate about coding. I have been laerning to code since 2010, and I worked in a company as a practiced software engineer. I often say "Coding is Live", and I want to build a scientific society. I believe that mathematic and algorithm can change the world!

#### 基本經歷
- 中山大學資工系在學
    - 於 CDPA 擔任課務
- 大安高工資訊科畢業
    - 於電腦研究社擔任教學長
- 現任 橘子蘋果兒童程式學苑 助教
- 曾任 Universe Tech 軟體工程師
- 曾任 Google Taiwan 實習生
- 曾任 奇蹟行動科技 軟體工程師實習生
- 學生生活 = 工作 + 實習 + 教學 + 競賽 + 社群 + 補足理論

#### 競賽經歷
- ACM-ICPC 亞洲賽 銅牌
- 全國大專電腦軟體設計競賽 佳作
- 全國網際網路程式競賽 優勝
- 奧林匹克數學競賽 三等獎
- 北市網界博覽會 第二名
- 數學公開邀請賽 第二名
- 北市軟體競賽開放組 第二名
- 北市軟體競賽高工組 佳作
- 全國技能競賽網頁設計 參賽
- 腦波創意競賽 最佳創意獎
	
#### 社群和活動經歷
- 擔任 SITCON 學生計算機年會 講者
- 擔任 INFAS 高中生學術聯展 講者
- 擔任 SITCON 學生計算機年會 議程組
- 擔任 SITCON 學生計算機年會夏令營 隊輔
- 擔任 SITCON 學生計算機年會夏令營 課活組
- 參加 COSCUP 開源人年會
- 參加 KSF2E 高雄前端開發者大會
- 資訊之芽培訓計畫算法班 優秀結業
- 參加 美國資訊科技產業見學團
- 參加黑客松台灣第三次主場
			
#### 教學經歷
- 家教 ─ 涵蓋國中數學、APCS、大學演算法與資料結構、基礎程式設計及其他
- 師大附中、中崙高中、和平高中、大安高中 ─ 網頁設計及版本控制系統
- 中山女中、大安高工 ─ 資訊安全
- 北一女中 ─ Processing 程式設計
- 大安高工 ─ C/C++
- 大安高工 ─ Algorithm and Data Structure
- 成發指導 ─ Python BOT
